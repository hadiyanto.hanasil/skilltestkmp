package smokeTest;

import Pages.*;
import common.DriverBuilder;
import org.junit.Test;

public class SmokeTest extends DriverBuilder {


    @Test
    /**
     * User able to see news
     */
    public void kmp_1(){
        HomePage homePage = new HomePage(getWebDriver());
        homePage.verifyHomepage();
        String newsTitle = homePage.clickFirstTrandingNews();
        NewsPage newsPage = new NewsPage(getWebDriver());
        newsPage.verifySpecificNewsPages(newsTitle);
    }

    @Test
    /**
     * User able to put comment
     */
    public void kmp_2(){
        HomePage homePage = new HomePage(getWebDriver());
        homePage.verifyHomepage();
        homePage.clickProfileIcon();
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage.verifyLoginPage();
        loginPage.clickGoogleButton();
        GoogleAuthPage googleAuthPage = new GoogleAuthPage(getWebDriver());
        String newWindow = moveNewWindow(getWebDriver());
        googleAuthPage.verifyGoogleAuthContent();
        googleAuthPage.loginGoogle("googleEmail", "googlePassword");
        backToMainWindow(getWebDriver(), newWindow);
        homePage.verifyHomepage();
        String newsTitle = homePage.clickFirstTrandingNews();
        NewsPage newsPage = new NewsPage(getWebDriver());
        newsPage.verifySpecificNewsPages(newsTitle);
        newsPage.inputComment("test comment");
        newsPage.clickSubmitCommentButton();

    }

    @Test
    /**
     * Guest user able to Register
     */
    public void kmp_3() {
        HomePage homePage = new HomePage(getWebDriver());
        homePage.verifyHomepage();
        homePage.clickProfileIcon();
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage.verifyLoginPage();
        loginPage.clickDaftarSekarangTextLink();
        RegisterPage registerPage = new RegisterPage(getWebDriver());
        registerPage.register("validEmail@email.com", "nameRegister");
        registerPage.verifyConfirmationModal();
        VerficationPage verficationPage = new VerficationPage(getWebDriver());
        verficationPage.verifyVerificationPage();
    }
    @Test
    /**
     * Error display when email or name is not valid
     */
    public void kmp_4() {
        HomePage homePage = new HomePage(getWebDriver());
        homePage.verifyHomepage();
        homePage.clickProfileIcon();
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage.verifyLoginPage();
        loginPage.clickDaftarSekarangTextLink();
        RegisterPage registerPage = new RegisterPage(getWebDriver());
        registerPage.register("Test", "Register");
        registerPage.verifyAlertPopup();

    }

    @Test
    /**
     * User able to login with google account
     * Ignore this test, because window google auth is not displayed after run test cases id kmp_2
     */
    public void kmp_5() {
        HomePage homePage = new HomePage(getWebDriver());
        homePage.verifyHomepage();
        homePage.clickProfileIcon();
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage.verifyLoginPage();
        loginPage.clickGoogleButton();
        GoogleAuthPage googleAuthPage = new GoogleAuthPage(getWebDriver());
        String newWindow = moveNewWindow(getWebDriver());
        googleAuthPage.verifyGoogleAuthContent();
        googleAuthPage.loginGoogle("validgoogleaccount", "validgoogleaccount");
        backToMainWindow(getWebDriver(), newWindow);
        homePage.verifyHomepage();

    }

    @Test
    /**
     * User able to login with facebook account
     */
    public void kmp_6() {
        HomePage homePage = new HomePage(getWebDriver());
        homePage.verifyHomepage();
        homePage.clickProfileIcon();
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage.verifyLoginPage();
        loginPage.clickFacebookButton();
        FacebookAuthPage facebookAuthPage = new FacebookAuthPage(getWebDriver());
        String newWindow = moveNewWindow(getWebDriver());
        facebookAuthPage.verifyFacebookAuthContent();
        facebookAuthPage.loginFacebook("validfacebookusername  ", "validfacebookpassword");
        backToMainWindow(getWebDriver(), newWindow);
        homePage.verifyHomepage();

    }

}
