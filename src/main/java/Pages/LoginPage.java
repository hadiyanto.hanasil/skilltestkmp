package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    private WebDriver webDriver;

    public LoginPage(WebDriver webDriver){
        this.webDriver=webDriver;
    }

    String loginContent = "[data-qa-id=\"main-section\"]";
    String facebookButton = "[data-qa-id=\"btn-login-fb\"]";
    String googleButton = "[data-qa-id=\"btn-login-google\"]";
    String daftarSekarangTextLink = "[href=\"/register\"]";

    public void verifyLoginPage() {
        timeout(10);
        webDriver.findElement(By.cssSelector(loginContent)).isDisplayed();
    }

    public void clickFacebookButton() {
        webDriver.findElement(By.cssSelector(facebookButton)).click();
    }

    public void clickGoogleButton() {
        webDriver.findElement(By.cssSelector(googleButton)).click();
    }

    public void clickDaftarSekarangTextLink() {
        webDriver.findElement(By.cssSelector(daftarSekarangTextLink)).click();
    }

    public void timeout(int timeoutInSecond){
        try {
            Thread.sleep(timeoutInSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
