package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleAuthPage {
    private WebDriver webDriver;

    public GoogleAuthPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }
    String googleAuthContent = "[role=\"presentation\"]";
    String emailTextField = "[type=\"email\"]";
    String nextButton = "[role=\"button\"]";
    String passwordTextField = "[type=\"password\"]";

    public void verifyGoogleAuthContent() {
        WebElement element = webDriver.findElement(By.cssSelector(googleAuthContent));
        WebDriverWait wait = new WebDriverWait(webDriver,20);
        wait.until(ExpectedConditions.visibilityOf(element));
        webDriver.findElement(By.cssSelector(googleAuthContent)).isDisplayed();
    }

    public void inputEmail(String username){
        webDriver.findElement(By.cssSelector(emailTextField)).sendKeys(username);
    }

    public void inputPassword(String password){
        timeout(10);
        webDriver.findElement(By.cssSelector(passwordTextField)).sendKeys(password);
    }

    public void clickNextButton(){
        webDriver.findElement(By.cssSelector(nextButton)).click();
    }

    public void loginGoogle(String username, String password){
        inputEmail(username);
        clickNextButton();
        inputPassword(password);
        clickNextButton();
    }

    public void timeout(int timeoutInSecond){
        try {
            Thread.sleep(timeoutInSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
