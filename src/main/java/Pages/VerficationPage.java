package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class VerficationPage {
    private WebDriver webDriver;

    public VerficationPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    String verificationText = "[class*=\"Textweb__StyledText\"]";

    public void verifyVerificationPage() {
        webDriver.findElement(By.cssSelector(verificationText)).isDisplayed();
    }

}
