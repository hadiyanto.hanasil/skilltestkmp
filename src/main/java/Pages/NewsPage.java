package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class NewsPage {
    private WebDriver webDriver;

    public NewsPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    String newsTitle = "[data-qa-id=\"story-title\"]";
    String commentTextField = "[data-qa-id=\"input-comment\"] > div > div";
    String submitCommentButton = "#track_submit_comment";

    public void verifySpecificNewsPages(String expectedTitle) {
        webDriver.findElement(By.cssSelector(newsTitle)).isDisplayed();
        webDriver.findElement(By.cssSelector(newsTitle)).getText().compareTo(expectedTitle);
    }

    public void inputComment(String comment){
        ((JavascriptExecutor) webDriver).executeScript("document.querySelector('body').scrollTop-=1400;");
        webDriver.findElement(By.cssSelector(commentTextField)).sendKeys(comment);
        timeout(40);
    }

    public void clickSubmitCommentButton() {
        timeout(40);
        webDriver.findElement(By.cssSelector(submitCommentButton)).click();
    }

    public void timeout(int timeoutInSecond){
        try {
            Thread.sleep(timeoutInSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
