package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FacebookAuthPage {
    private WebDriver webDriver;

    public FacebookAuthPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }
    String facebookAuthContent = "[class=\"fb_content clearfix\"]";
    String usernameTextField = "[type=\"text\"]";
    String passwordTextField = "[type=\"password\"]";
    String loginButton = "[name=\"login\"]";

    public void verifyFacebookAuthContent(){
        webDriver.findElement(By.cssSelector(facebookAuthContent)).isDisplayed();
    }

    public void inputUsername(String username){
        webDriver.findElement(By.cssSelector(usernameTextField)).sendKeys(username);
    }

    public void inputPassword(String password){
        webDriver.findElement(By.cssSelector(passwordTextField)).sendKeys(password);
    }

    public void clickLoginButton(){
        webDriver.findElement(By.cssSelector(loginButton)).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void loginFacebook(String username, String password){
        inputUsername(username);
        inputPassword(password);
        clickLoginButton();
    }

}
