package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.UUID;

public class RegisterPage {
    private WebDriver webDriver;

    public RegisterPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    String emailTextField = "[name=\"email\"]";
    String nameTextField = "[name=\"name\"]";
    String daftarButton = "[data-qa-id=\"btn-save\"]";
    String confirmationModal = "[class*=\"ConfirmationModal__Modal\"]";
    String alertPopup = "[data-qa-id=\"alert-msg\"]";

    public void inputEmailTextField(String email){
        webDriver.findElement(By.cssSelector(emailTextField)).sendKeys(email);
    }

    public void inputNameTextField(String name){
        webDriver.findElement(By.cssSelector(nameTextField)).sendKeys(name);
    }

    public void clickDaftarButton(){
        webDriver.findElement(By.cssSelector(daftarButton)).click();
    }

    public void verifyConfirmationModal() {
        webDriver.findElement(By.cssSelector(confirmationModal)).isDisplayed();
    }

    public void verifyAlertPopup(){
        webDriver.findElement(By.cssSelector(alertPopup)).isDisplayed();
    }
    public void register(String email, String name){
        String uuid = UUID.randomUUID().toString();
        System.out.println(uuid);
        inputEmailTextField(uuid+email);
        inputNameTextField(name);
        clickDaftarButton();
    }
}
