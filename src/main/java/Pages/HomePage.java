package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    public WebDriver webDriver;

    public HomePage (WebDriver webDriver) {
        this.webDriver=webDriver;
    }

    String homepageContent = "[data-qa-id=\"news-card\"]";
    String profileIcon = "[data-qa-id=\"hd-login\"]";
    String firstTrandingNews = "//*[@data-qa-id=\"trending-section\"]/div/div/div[3]/div/div[1]/div/div[2]//h2/a/span";

    public void verifyHomepage(){
        timeout(20);
        WebElement element = webDriver.findElement(By.cssSelector(homepageContent));
        WebDriverWait wait = new WebDriverWait(webDriver,30);
        wait.until(ExpectedConditions.visibilityOf(element));
        webDriver.findElement(By.cssSelector(homepageContent)).isDisplayed();
    }
    public void clickProfileIcon() {
        webDriver.findElement(By.cssSelector(profileIcon)).isDisplayed();
        webDriver.findElement(By.cssSelector(profileIcon)).click();
    }

    public String clickFirstTrandingNews() {
        webDriver.findElement(By.xpath(firstTrandingNews)).isDisplayed();
        String newsTitle = getFirstTrandingTitle();
        webDriver.findElement(By.xpath(firstTrandingNews)).click();
        return newsTitle;
    }

    private String getFirstTrandingTitle() {
        webDriver.findElement(By.xpath(firstTrandingNews)).getText();
        return webDriver.findElement(By.xpath(firstTrandingNews)).getText();
    }

    public void timeout(int timeoutInSecond){
        try {
            Thread.sleep(timeoutInSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
